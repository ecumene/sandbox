#include /assets/shader/default.fsh;

uniform vec3 La; // Ambient scene light

void main(){
	gl_FragColor = vec4(La, 1.0) * texture2D(material.Md, Vt);
}