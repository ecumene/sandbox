varying vec3 Vp;            // The vertex position in object / eye space
varying vec2 Vt;            // The vertex texture mapping coordinate

varying vec2 Sr;            // Screen resolution

struct Material
{
	sampler2D Md;
	sampler2D Ms;
	sampler2D Mn;
	
	vec3 Ka;
	vec3 Ks;
	vec3 Kd;
	
	float shininess;
};

uniform Material material; // Material input