#include /assets/shader/default.vsh;

void main(){
	gl_Position = Mp * Mv * Mo * gl_Vertex;
	
	Vp = vec3(Mv * Mo * gl_Vertex);
	Vt = gl_MultiTexCoord0.st;
}