#include /assets/shader/default.fsh;

varying vec3 Lp;
varying vec3 Lc;

void main(void) 
{
	vec3 Ps = texture2D(material.Ms, Vt).rgb * vec3(material.shininess);
	vec3 Pn = texture2D(material.Mn, Vt).rgb * 2.0 - 1.0;
	vec3 L = normalize(vec3((Lp.xy - gl_FragCoord.xy).xy / Sr.xy, Lp.z));
	vec3 N = normalize(Pn);
	
	float Idiff;
	float Ispec;
	
	Idiff = max(dot(L, N), 0.0);
	if(Idiff > 0.0){	
		vec3 SLN = normalize(vec3(Lp.x, Sr.y - Lp.y, Lp.z).xyz - vec3(Vp));
		Ispec = vec4(material.Ks, 1) * vec4(pow(max(dot(reflect(SLN, N), -SLN), 0.0), Ps.r));
	}
	
	float a = 1.0 / length(vec2(Lp.xy - gl_FragCoord.xy));
	
	gl_FragColor = (vec4(Lc, 1) * vec4(a, a, a, pow(a, 3.0))) * ((vec4(material.Kd, 1) * vec4(vec3(Idiff), 1.0)) + (vec4(material.Ks, 1.0) * vec4(vec3(Ispec), 1.0)));
}