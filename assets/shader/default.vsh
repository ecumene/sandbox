varying vec3 Vp; // The vertex position in object / eye space
varying vec2 Vt; // The vertex texture coordinate

varying vec2 Sr; // Screen resolution

uniform mat4 Mp; // The projection matrix
uniform mat4 Mv; // The view matrix
uniform mat4 Mo; // The object matrix
uniform vec2 screenResolution; // Screen resolution