#include /assets/shader/default.vsh;

struct Light
{
	vec3 position;
	vec3 color;
};

uniform Light light;

varying vec3 Lp;
varying vec3 Lc;

void main(){
	gl_Position = Mp * Mv * Mo * gl_Vertex;
	
	Sr = screenResolution;
	
	Lp = vec4(light.position.x, Sr.y - light.position.y, light.position.z, 1);
	Lc = light.color;
	
	Vp = vec3(Mv * Mo * gl_Vertex);
	Vt = gl_MultiTexCoord0.st;
}