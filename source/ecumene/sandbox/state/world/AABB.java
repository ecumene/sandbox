package ecumene.sandbox.state.world;

import org.lwjgl.util.vector.Vector2f;

public class AABB
{
   public Vector2f position, size;
   
   public AABB(Vector2f pos, Vector2f size)
   {
      this.position = pos;
      this.size = size;
   }
   
   public static boolean collides(AABB a, AABB b)
   {
      if(Math.abs(a.position.x - b.position.x) < a.size.x + b.size.x)
      {
         if(Math.abs(a.position.y - b.position.y) < a.size.y + b.size.y)
         {
            return true;
         }
      }
      
      return false;
   }
   
   public static boolean inside(AABB a, Vector2f b)
   {
      if(Math.abs(a.position.x - b.x) < a.size.x)
      {
         if(Math.abs(a.position.y - b.y) < a.size.y)
         {
            return true;
         }
      }
      return false;
   }
}