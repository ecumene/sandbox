package ecumene.sandbox.state.world;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import ecumene.data.Transform;
import ecumene.opengl.shader.Program;

public class CameraComponent extends WorldComponent {
	
	private Vector3f position;
	private Vector3f rotation;
	
	private Transform projection;
	private Transform view;
	
	public CameraComponent(float speed, float mouse){
		rotation = new Vector3f();
		position = new Vector3f();
		
		projection = new Transform();
		view = new Transform();
				
		projection.toOrthographic(0, Display.getWidth(), Display.getHeight(), 0, -1, 1000);
	}

	public void init(WorldContext world) throws Throwable {}
	public void destroy(WorldContext world) throws Throwable {}
	
	@Override
	public void render(WorldContext world, Program shader) throws Throwable {
		shader.setUniformM4("Mp", false, projection());
		shader.setUniformM4("Mv", false, view());
	}

	@Override
	public void update(WorldContext world, long delta) throws Throwable {
		view.setIdentity();
		view.rotate(rotation.x, new Vector3f(1, 0, 0));
		view.rotate(rotation.y, new Vector3f(0, 1, 0));
		view.rotate(rotation.z, new Vector3f(0, 0, 1));
		view.translate(new Vector3f(position.x, position.y, position.z));
	}
	
	public Transform projection(){
		return projection;
	}
	
	public Transform view() {
		return view;
	}
}
