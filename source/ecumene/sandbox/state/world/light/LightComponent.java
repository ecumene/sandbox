package ecumene.sandbox.state.world.light;

import org.lwjgl.util.vector.Vector3f;

import ecumene.EcuException;
import ecumene.data.Color;
import ecumene.geom.Material;
import ecumene.opengl.Window;
import ecumene.opengl.shader.Program;
import ecumene.sandbox.state.world.WorldComponent;
import ecumene.sandbox.state.world.WorldContext;

public abstract class LightComponent extends WorldComponent {	
	public Vector3f position;
	public Color color;
	
	public LightComponent(Vector3f position, Color color) {
		this.position = position;
		this.color = color;
	}
	
	public void render(WorldContext context, WorldComponent component) throws Throwable {
		getLightProgram().use();
		
		// Light uniforms
		getLightProgram().setUniformf("light.position", position.x, position.y, position.z);
		getLightProgram().setUniformc3("light.color", color);
		
		if(!(component instanceof LightComponent) && component.getMaterial() != null){
			// Material uniforms
			if(component.getMaterial().map_Kd != null) getLightProgram().setUniformi("material.Md", WorldContext.MAP_DIFFUSE_SAMPLER);
			if(component.getMaterial().map_Ks != null) getLightProgram().setUniformi("material.Ms", WorldContext.MAP_SPECULAR_SAMPLER);
			if(component.getMaterial().map_Kn != null) getLightProgram().setUniformi("material.Mn", WorldContext.MAP_NORMAL_SAMPLER);
			
			getLightProgram().setUniformf("material.Ka", component.getMaterial().getAmbient().r, component.getMaterial().getAmbient().g, component.getMaterial().getAmbient().b);
			getLightProgram().setUniformf("material.Ks", component.getMaterial().getSpecular().r, component.getMaterial().getSpecular().g, component.getMaterial().getSpecular().b);
			getLightProgram().setUniformf("material.Kd", component.getMaterial().getDiffuse().r, component.getMaterial().getDiffuse().g, component.getMaterial().getDiffuse().b);
			
			getLightProgram().setUniformf("material.shininess", component.getMaterial().getSpecularPower());
			getLightProgram().setUniformf("screenResolution", Window.getWidth(), Window.getHeight());
		}
		
		if(!(component instanceof LightComponent)){
			component._render(context, getLightProgram());
		}
	}
	
	@Override
	public void init(WorldContext world) throws Throwable {}
	@Override
	public void render(WorldContext world, Program shader) throws Throwable {}
	@Override
	public void update(WorldContext world, long delta) throws Throwable {}
	@Override
	public void destroy(WorldContext world) throws Throwable {}
	@Override
	public Material getMaterial() {throw new EcuException("Cannot use materials for lights");}
	public abstract Program getLightProgram();
}