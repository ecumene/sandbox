package ecumene.sandbox.state.world.light;

import org.lwjgl.util.vector.Vector3f;

import ecumene.data.Color;
import ecumene.opengl.shader.Program;
import ecumene.opengl.shader.ShaderAsset;
import ecumene.sandbox.state.world.WorldContext;

public class PointLightComponent extends LightComponent {
	private static Program program;

	public PointLightComponent(WorldContext context, Vector3f position, Color color) {
		super(position, color);
		if (program == null) {
			program = new Program();
			program.attachShader((ShaderAsset) context.getAssetLoader().getAsset("point-light-fs"));
			program.attachShader((ShaderAsset) context.getAssetLoader().getAsset("point-light-vs"));
			program.link();
		}
	}

	@Override
	public Program getLightProgram() {
		return program;
	}
}
