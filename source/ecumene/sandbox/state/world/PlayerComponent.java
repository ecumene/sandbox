package ecumene.sandbox.state.world;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import ecumene.data.Color;
import ecumene.geom.Face;
import ecumene.geom.GeomBuffer;
import ecumene.geom.Material;
import ecumene.geom.Model;
import ecumene.geom.Vertex;
import ecumene.opengl.GLPrimitive;
import ecumene.opengl.shader.Program;
import ecumene.opengl.texture.ITexture;
import ecumene.opengl.texture.ImageAsset;
import ecumene.sandbox.runtime.GameLauncher;

public class PlayerComponent extends WorldComponent {
	private static Model model;
	private static GeomBuffer geometry;
	
	private ITexture diffuse;
	private ITexture specular;
	private ITexture normal;
	
	private int anim_still = 0;
	private int anim_walk1 = 1;
	private int anim_walk2 = 2;
		
	private int anim_state_curr = 0;
	private int anim_state_last = 1;
			
	public PlayerComponent() {
		super();
	}
	
	@Override
	public void init(WorldContext world) throws Throwable {
		if(model == null){
			model = new Model(GLPrimitive.TRIANGLES);
			model.add(0, new Vertex(new Vector4f(00, 64, 0, 1), new Vector2f(0, 1), new Vector3f(0, 0, 0)));
			model.add(1, new Vertex(new Vector4f(00, 00, 0, 1), new Vector2f(0, 0), new Vector3f(0, 0, 0)));
			model.add(2, new Vertex(new Vector4f(64, 00, 0, 1), new Vector2f(1, 0), new Vector3f(0, 0, 0)));
			model.add(3, new Vertex(new Vector4f(64, 64, 0, 1), new Vector2f(1, 1), new Vector3f(0, 0, 0)));
			model.addFace(0, new Face(0, 1, 2, 2, 3, 0));
		}
		
		geometry = new GeomBuffer();
		geometry.add(model);
		geometry.upload();
		
		anim_state_curr = anim_still;
		
		dimension = new Vector2f(64, 64);
		scale      = new Vector3f(1.5f, 1.5f, 1f);
		
		specular = (ImageAsset) world.getAssetLoader().getAsset("default_specular");
		
		material = new Material();
		material.setAmbient(new Color(1, 1, 1, 1));
		material.setDiffuse(new Color(1, 1, 1, 1));
		material.setSpecular(new Color(1, 1, 1, 1));
		material.setSpecularPower(1f);
	}
	
	@Override
	public void render(WorldContext world, Program shader) throws Throwable {
		if(anim_state_curr != anim_state_last){
			switch(anim_state_curr){
			case 0: {
				diffuse =  (ImageAsset) world.getAssetLoader().getAsset("player_still_d");
				normal =   (ImageAsset) world.getAssetLoader().getAsset("player_still_n");
				break;
			}
			
			case 1: {
				diffuse =  (ImageAsset) world.getAssetLoader().getAsset("player_walk1_d");
				normal =   (ImageAsset) world.getAssetLoader().getAsset("player_walk1_n");
				break;
			}
			
			case 2: {
				diffuse =  (ImageAsset) world.getAssetLoader().getAsset("player_walk2_d");
				normal =   (ImageAsset) world.getAssetLoader().getAsset("player_walk2_n");
				break;
			} 
			}
			
			material.map_Kd = (ITexture) diffuse;
			material.map_Kn = (ITexture) normal;
			material.map_Ks = (ITexture) specular;
		}
		
		anim_state_last = anim_state_curr;
		
		diffuse.bind();
		specular.bind();
		normal.bind();
		
		geometry.render();
	}
	
	private int left = 0, right = 1;
	private int curr_direction = right;
	private int last_direction = left;
	
	private float animationTick;
	private float updateItr;
	private boolean moving;
	private float yMod;
	private float yTemp;
	private float lastScaleX;
	
	@Override
	public void update(WorldContext world, long delta) throws Throwable {
		float speed = 0.185f * (float) delta;
		
		if(last_direction != curr_direction){
			if(curr_direction == left){
				position.x -= (64*0-lastScaleX);
			}
			
			if(curr_direction == right){
				position.x -= (64*0-lastScaleX);
			}
		}	
				
		lastScaleX = scale.x;
		last_direction = curr_direction;
		
		if(Keyboard.isKeyDown(GameLauncher.getInstance().getWorldContext().getSettings().key_move_left)){
			position.x -= speed;
			curr_direction = left;
			moving = true;
		}
		
		if(Keyboard.isKeyDown(GameLauncher.getInstance().getWorldContext().getSettings().key_move_right)){
			position.x += speed;
			curr_direction = right;
			moving = true;
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_S)){
			position.y += speed;
		}
				
		if(moving){
			if((animationTick < 1)){
				anim_state_curr = anim_walk1;
			} else if((animationTick >= 1 && animationTick <= 2)){
				anim_state_curr = anim_still;
			} else if((animationTick >= 2 && animationTick <= 3)){
				anim_state_curr = anim_walk2;
			} else if((animationTick >= 3 && animationTick <= 4)){
				anim_state_curr = anim_still;
			}
			
			if(animationTick >= 4) animationTick = 0;
			animationTick += 0.008f * (float) delta;
			
			updateItr += 0.028f * (float) delta; // Bouncy-ness
			yMod = (float) Math.sin(updateItr) * 4; // 4 = Intensity
		} else {
			yMod = 0;
			anim_state_curr = anim_still;
		}
		
		
		yTemp = position.y + yMod;
		
		position.y = yTemp;
		
		moving = false;
	}

	@Override
	public void destroy(WorldContext world) throws Throwable {
		transform = null;
	}
}
