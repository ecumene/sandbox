package ecumene.sandbox.state.world;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL42;
import org.lwjgl.util.vector.Vector3f;

import ecumene.asset.AssetLoader;
import ecumene.data.Color;
import ecumene.opengl.Window;
import ecumene.opengl.frame.Frame;
import ecumene.opengl.shader.IShader;
import ecumene.opengl.shader.Program;
import ecumene.opengl.shader.ShaderAsset;
import ecumene.opengl.texture.ImageAsset;
import ecumene.opengl.texture.Texture;
import ecumene.sandbox.state.GameContext;
import ecumene.sandbox.state.world.light.LightComponent;
import ecumene.sandbox.state.world.light.PointLightComponent;

public class WorldContext extends GameContext {
	private FloatBuffer clearColor;
	private FloatBuffer clearDepth;
	
	private AssetLoader loader;
	private Program ambient;
	private Program post;
	private CameraComponent camera;
	
	private List<WorldComponent> components;
	
	// Some static constants
	public static final float CAMERA_SPEED = 0.025f;
	public static final float CAMERA_MOUSE = 0.005f;
	
	public static final int MAP_DIFFUSE_SAMPLER  = 0;
	public static final int MAP_SPECULAR_SAMPLER = 1;
	public static final int MAP_NORMAL_SAMPLER   = 2;
	
	private PointLightComponent mouseLight;
	private WorldConstants settings;
	
	private Frame frame;
		
	public WorldContext() {
		loader = new AssetLoader();
		loader.add("world-ambient-shader-fs", new ShaderAsset(new File("./assets/shader/ambient.fs"), GL20.GL_FRAGMENT_SHADER));
		loader.add("world-ambient-shader-vs", new ShaderAsset(new File("./assets/shader/ambient.vs"), GL20.GL_VERTEX_SHADER));
		
		loader.add("post-process-frame-fs", new ShaderAsset(new File("./assets/shader/post.fs"), GL20.GL_FRAGMENT_SHADER));
		loader.add("post-process-frame-vs", new ShaderAsset(new File("./assets/shader/post.vs"), GL20.GL_VERTEX_SHADER));
		
		loader.add("point-light-fs", new ShaderAsset(new File("./assets/shader/point-light.fs"), GL20.GL_FRAGMENT_SHADER));
		loader.add("point-light-vs", new ShaderAsset(new File("./assets/shader/point-light.vs"), GL20.GL_VERTEX_SHADER));
		
		loader.add("default_specular", new ImageAsset(new File("./assets/texture/default-specular.png"), MAP_SPECULAR_SAMPLER,  GL11.GL_NEAREST));
		loader.add("default_normal",   new ImageAsset(new File("./assets/texture/default-normal.png"),   MAP_NORMAL_SAMPLER,  GL11.GL_NEAREST));
		
		loader.add("player_still_d", new ImageAsset(new File("./assets/texture/player/player_still_d.png"), MAP_DIFFUSE_SAMPLER,  GL11.GL_NEAREST));
		loader.add("player_still_n", new ImageAsset(new File("./assets/texture/player/player_still_n.png"), MAP_NORMAL_SAMPLER,   GL11.GL_NEAREST));

		loader.add("player_walk1_d", new ImageAsset(new File("./assets/texture/player/player_walk1_d.png"), MAP_DIFFUSE_SAMPLER,  GL11.GL_NEAREST));
		loader.add("player_walk1_n", new ImageAsset(new File("./assets/texture/player/player_walk1_n.png"), MAP_NORMAL_SAMPLER,   GL11.GL_NEAREST));
		
		loader.add("player_walk2_d", new ImageAsset(new File("./assets/texture/player/player_walk2_d.png"), MAP_DIFFUSE_SAMPLER,  GL11.GL_NEAREST));
		loader.add("player_walk2_n", new ImageAsset(new File("./assets/texture/player/player_walk2_n.png"), MAP_NORMAL_SAMPLER,   GL11.GL_NEAREST));		
		
		components = new ArrayList<WorldComponent>();
		
		clearColor = BufferUtils.createFloatBuffer(4);
		clearColor.put(new float[]{1f, 1f, 1f, 1f});
		clearColor.flip();
		
		clearDepth = BufferUtils.createFloatBuffer(4);
		clearDepth.put(new float[]{1, 1, 1, 1});
		clearDepth.flip();
		
		settings = new WorldConstants();
	}
	
	@Override
	public void init() throws Throwable {
		components.clear();
		loader.load();
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		
		camera = new CameraComponent(CAMERA_SPEED, CAMERA_MOUSE);
		
		ambient = new Program();
		ambient.attachShader((IShader) loader.getAsset("world-ambient-shader-fs"));
		ambient.attachShader((IShader) loader.getAsset("world-ambient-shader-vs"));
		ambient.link();
		
		post = new Program();
		post.attachShader((IShader) loader.getAsset("post-process-frame-fs"));
		post.attachShader((IShader) loader.getAsset("post-process-frame-vs"));
		post.link();
		
		mouseLight = new PointLightComponent(this, new Vector3f(20, 20, 0.075f), new Color(64, 64, 64, 1));
				
		components.add(camera);
		components.add(new PlayerComponent());
		components.add(mouseLight);

		Texture tempTexture = new Texture(GL11.GL_TEXTURE_2D);
		tempTexture.bind();
		GL42.glTexStorage2D(GL11.GL_TEXTURE_2D, 1, GL11.GL_RGBA8, 512, 512);
		
		frame = new Frame();
		frame.bind();
		frame.bufferTexture(tempTexture, GL30.GL_COLOR_ATTACHMENT0);
		
		for(int i = 0; i < components.size(); i++){
			components.get(i).init(this);
		}
	}
	
	
	@Override
	public void render() throws Throwable {
		// Clear color (white)
		GL30.glClearBuffer(GL11.GL_COLOR, 0, clearColor);
		
		ambient.use();
		ambient.setUniformf("La", 1f, 1f, 1f);
		
		for (int i = 0; i < components.size(); i++) {
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			
			if(components.get(i) instanceof LightComponent){
				for(int ci = 0; ci < components.size(); ci++){
					((LightComponent) components.get(i)).render(this, components.get(ci));
				}
			} else {
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				components.get(i)._render(this, ambient);
			}
		}
	}

	@Override
	public void update(long delta) throws Throwable {
		mouseLight.position = new Vector3f(Mouse.getX(), Window.getHeight() - Mouse.getY(), 0.0075f);
		
		for(int i = 0; i < components.size(); i++){
			components.get(i)._update(this, delta);
		}
	}

	@Override
	public void destroy() throws Throwable {
		for(int i = 0; i < components.size(); i++){
			components.get(i).destroy(this);
		}
		
		ambient.destroy();
		loader.unload();
	}

	@Override
	public AssetLoader getAssetLoader()  {
		return loader;
	}

	public CameraComponent getCamera() {
		return camera;
	}
	
	public WorldConstants getSettings(){
		return settings;
	}
}
