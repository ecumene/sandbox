package ecumene.sandbox.state.world;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import ecumene.data.Transform;
import ecumene.geom.Material;
import ecumene.opengl.shader.Program;

public abstract class WorldComponent {
	protected Material material;
	protected Transform transform;
	
	protected Vector3f position;
	protected Vector3f scale;
	protected Vector3f rotation;
	protected Vector2f dimension;
	
	protected AABB bounding;
	
	public WorldComponent(){
		dimension = new Vector2f(1, 1);
		
		transform = new Transform();
		position = new Vector3f(0, 0, 0);
		scale    = new Vector3f(1, 1, 1);
		rotation = new Vector3f(0, 0, 0);
		
		bounding = new AABB(new Vector2f(position.x, position.y), dimension);
	}
	
	public void _render(WorldContext world, Program shader) throws Throwable{
		shader.setUniformi("material.Md", 0);
		shader.setUniformM4("Mo", false, transform);
//		shader.setUniformM4("Mn", true, Matrix4f.invert(Matrix4f.mul(world.getCamera().view(), transform, null), null));
		
		render(world, shader);
	}
	
	public void _update(WorldContext world, long delta) throws Throwable{
		bounding.size = new Vector2f((dimension.x/2) * scale.x, (dimension.y/2) * scale.y);		
		bounding.position.x = position.x + ((dimension.x/2) * scale.x);
		bounding.position.y = position.y + ((dimension.y/2) * scale.y);
		
		update(world, delta);

		transform.setIdentity();
		transform.translate(position);
		transform.rotate(rotation.x, new Vector3f(1, 0, 0));
		transform.rotate(rotation.y, new Vector3f(0, 1, 0));
		transform.rotate(rotation.z, new Vector3f(0, 0, 1));
		transform.scale(scale);
	}
	
	public abstract void init(WorldContext world) throws Throwable;
	
	public abstract void render(WorldContext world, Program shader) throws Throwable;
	public abstract void update(WorldContext world, long delta) throws Throwable;
	
	public abstract void destroy(WorldContext world) throws Throwable;
	
	public Transform getTransform(){
		return transform;
	}
	
	public Material getMaterial(){
		return material;
	}
}
