package ecumene.sandbox.state;

import ecumene.asset.AssetLoader;

// Represents a state the game could be in, and
// what remains constant between it

public abstract class GameContext {
	protected int index;
	
	public abstract void init() throws Throwable;
	public abstract void render() throws Throwable;
	public abstract void update(long delta) throws Throwable;
	public abstract void destroy() throws Throwable;
	
	public abstract AssetLoader getAssetLoader();
}