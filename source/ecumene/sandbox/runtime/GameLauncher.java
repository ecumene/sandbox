package ecumene.sandbox.runtime;

import java.io.File;
import java.io.IOException;

import ecumene.EcuSystem;
import ecumene.logging.Logger;
import ecumene.opengl.Window;
import ecumene.opengl.WindowAttribs;
import ecumene.sandbox.state.GameContext;
import ecumene.sandbox.state.world.WorldContext;
import ecumene.sandbox.timing.Timer;

// The main class, does things that are
// constant regardless of the game's
// current context

public class GameLauncher {
	private static String[] cArgs;
	private static Logger logging;

	WindowAttribs windowAttributes;
	private GameArgumentInterp argumentInterp;
	private boolean running;
	private Timer timer;
	private static GameLauncher LAUNCHER;
	
	private GameContext context;
	private WorldContext worldContext;
	
	private GameLauncher(String[] cArgs) throws IOException {
		argumentInterp = new GameArgumentInterp(logging);
		GameLauncher.cArgs = cArgs;
		worldContext = new WorldContext();
	}
	
	private void init() throws Throwable {
		logging.log("sinfo", "Starting the sandbox game...");
		logging.log("sinfo", "System info: ");
		logging.log("-----", " ... ");
		logging.log("sinfo", "java:   " + EcuSystem.JAVA_VERSION + ", " + EcuSystem.JAVA_VENDOR);
		logging.log("sinfo", "system: " + EcuSystem.OS);
		logging.log("-----", " ... ");
		
		argumentInterp.interporate(this, cArgs);
		Window.create(windowAttributes);
		timer = new Timer();
		beginContext(worldContext);
		
		running = true;
		while(running){
			running = !Window.isClosing();
			long delta = timer.stepUpdate();
			
			context.render();
			context.update(delta);
			
			Window.update();
			timer.stepFrame();
		}
		
		context.destroy();
		Window.destroy();
		System.exit(0);
	}
	
	private void beginContext(GameContext context) throws Throwable{
		if(this.context != null) this.context.destroy();
		this.context = context;
		this.context.init();
	}
	
	public WorldContext getWorldContext(){
		return worldContext;
	}
	
	public static GameLauncher getInstance(){
		return LAUNCHER;
	}
	
	public static void main(String[] cArgs) {
		try {
			logging = new Logger(new File("./logs/main.log"), true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			GameLauncher launcher = new GameLauncher(cArgs);
			LAUNCHER = launcher;
			launcher.init();
		} catch (Throwable e) {
			logging.log("strce", "// Beginning stack trace, cause and message may point null");
			logging.log("strce", "{");
			logging.log("strce", "\"" + e.getClass() + "\"" + " says " + "\"" + e.getLocalizedMessage() + "\"");
			for(StackTraceElement element : e.getStackTrace()){
				logging.log("strce", "-   call " + element.getClassName() + "." + element.getMethodName() + "(" + element.getFileName() + ":" + element.getLineNumber() + ")");
			}
			logging.log("strce", "}");
			logging.log("strce", "// End stack trace");
		}
		
		logging.flush();
	}
}