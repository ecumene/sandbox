package ecumene.sandbox.runtime;

import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;

import ecumene.logging.Logger;
import ecumene.opengl.WindowAttribs;

// Reads runtime arguments and parses them,
// then saves them to the given game launcher.
// - Isolated from everything else because
// - it would be too messy just sitting in the 
// - game launcher

public class GameArgumentInterp {
	private Logger logging;
	
	public GameArgumentInterp(Logger logging) {
		this.logging = logging;
	}
	
	public void interporate(GameLauncher launcher, String[] rtArguments) {
		DisplayMode displayMode = null;
		int width = 800, height = 600;
		
		if(rtArguments.length > 0){
			logging.log("rtarg", "Found command line arguments...");
			
			boolean acceptedArgument;
			
			for(int i = 0; i < rtArguments.length; i++){
				acceptedArgument = false;
				
				if(rtArguments[i].equalsIgnoreCase("WINDOW")){	
					if(canParseInt(rtArguments[i+1])){ width  = Integer.parseInt(rtArguments[i+1]); }
					if(canParseInt(rtArguments[i+2])){ height = Integer.parseInt(rtArguments[i+2]); }
				}
				
				if(acceptedArgument) logging.log("rtarg", "Interporated argument: " + rtArguments[i]);
				else                 logging.log("rtarg", "Failed to interporate argument: " + rtArguments);
			}
			
			logging.log("rtarg", "End command line arguments");
		}
		
		displayMode = new DisplayMode(width, height);
		
		launcher.windowAttributes = new WindowAttribs();
		launcher.windowAttributes.displayMode = displayMode;
		launcher.windowAttributes.pixelFormat = new PixelFormat();
	}
	
	private boolean canParseInt(String i){
		try {
			Integer.parseInt(i);
		} catch (NumberFormatException e){
			return false;
		}
		return false;
	}
}
