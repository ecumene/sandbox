package ecumene.sandbox.timing;

public class Timer {
	private long completeFrames  = 0;  // How many frames have been successfully processed
	
	private long currentFrameTime = 0; // Time at the frame we're processing now
	private long lastFrameTime    = 0; // Time at the last frame
	private long deltaTime        = 1; // Time between the two
	
	private long lastFPS;   // Time at last FPS
	private int frames;     // The frames processed in this second
	private int currentFPS; // Frames per second, updated every time a second has passed
	
	public Timer() {
		lastFPS = System.currentTimeMillis();
		lastFrameTime = System.currentTimeMillis();
	}
	
	public long stepUpdate(){
		lastFrameTime = currentFrameTime;
		currentFrameTime = System.currentTimeMillis();
		deltaTime = currentFrameTime - lastFrameTime;
		
		if(System.currentTimeMillis() - lastFPS > 1000){
			currentFPS = frames;
			frames = 0;
			lastFPS += 1000;
		}
		
		frames++;
		
		return deltaTime;
	}
	
	// Long because calculating 9 quintillion frames is kinda realistic...
	// Damn kids and their 2^63-1 frame caps
	
	public long getFrames(){
		return completeFrames;
	}
	
	public int getConstFPS(){
		return currentFPS;
	}
	
	public void stepFrame(){
		completeFrames++;
	}
	
	public long getDelta(){
		return deltaTime;
	}
}
